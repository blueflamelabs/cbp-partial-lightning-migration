<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Pay_by_Credit_Card</fullName>
        <description>Pay by Credit Card</description>
        <protected>false</protected>
        <recipients>
            <field>Billing_Contact_1__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Billing_Contact_2__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>admincb@campbelknap.org</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Deposit_by_Credit_Card</template>
    </alerts>
    <alerts>
        <fullName>Send_Check_Deposit_Instructions</fullName>
        <description>Send Check Deposit Instructions</description>
        <protected>false</protected>
        <recipients>
            <field>Billing_Contact_1__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Billing_Contact_2__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>admincb@campbelknap.org</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Deposit_by_Check_Instructions</template>
    </alerts>
    <fieldUpdates>
        <fullName>Date_Canceled_TODAY</fullName>
        <field>Date_Canceled__c</field>
        <formula>TODAY()</formula>
        <name>Date Canceled = TODAY</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Date_Off_Waitlist_BLANK</fullName>
        <field>Date_Off_Waitlist__c</field>
        <name>Date Off Waitlist = BLANK</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Date_Off_Waitlist_TODAY</fullName>
        <field>Date_Off_Waitlist__c</field>
        <formula>TODAY()</formula>
        <name>Date Off Waitlist = TODAY</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Date_Placed_TODAY</fullName>
        <field>Date_Placed__c</field>
        <formula>TODAY()</formula>
        <name>Date Placed = TODAY</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Date_Waitlisted_TODAY</fullName>
        <field>Date_Waitlisted__c</field>
        <formula>TODAY()</formula>
        <name>Date Waitlisted = TODAY</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Deposit_SKU</fullName>
        <field>Deposit_SKU__c</field>
        <formula>Id</formula>
        <name>Deposit SKU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Late_Fee_Amount</fullName>
        <field>Late_Fee__c</field>
        <formula>Camp_Year__r.Late_Fee__c</formula>
        <name>Late Fee Amount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Late_Fee_Amount_Removed</fullName>
        <field>Late_Fee__c</field>
        <name>Late Fee Amount Removed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Deposit SKU</fullName>
        <actions>
            <name>Deposit_SKU</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Application__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>If Application Status %3D Accepted%3B Date Placed %3C%3E NULL</fullName>
        <actions>
            <name>Date_Placed_TODAY</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Application__c.Status__c</field>
            <operation>equals</operation>
            <value>Accepted</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>If Application Status %3D Canceled%3B Date Placed %3C%3E NULL</fullName>
        <actions>
            <name>Date_Canceled_TODAY</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Application__c.Status__c</field>
            <operation>equals</operation>
            <value>Canceled</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>If Application Status %3D Waitlist%3B Date Waitlisted %3C%3E NULL</fullName>
        <actions>
            <name>Date_Waitlisted_TODAY</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Application__c.Status__c</field>
            <operation>equals</operation>
            <value>Waitlist</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>If Application Status PRIORVALUE %3D Waitlist%3B Date Off Waitlist %3C%3E NULL</fullName>
        <actions>
            <name>Date_Off_Waitlist_TODAY</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(NOT(ISNEW()),(ISPICKVAL(PRIORVALUE
(Status__c), 
&quot;Waitlist&quot;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Late Fee Applied</fullName>
        <actions>
            <name>Late_Fee_Amount</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Application__c.Late_Fee_Assessed__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Applies late fee when Late_Fee_Assessed__c = TRUE</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Late Fee Removed</fullName>
        <actions>
            <name>Late_Fee_Amount_Removed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Application__c.Late_Fee_Assessed__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Reomves late fee when Late_Fee_Assessed__c = FALSE</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Preferred Deposit Method %3D Check</fullName>
        <actions>
            <name>Send_Check_Deposit_Instructions</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Application__c.Preferred_Deposit_Method__c</field>
            <operation>equals</operation>
            <value>Check</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Preferred Deposit Method %3D Credit Card</fullName>
        <actions>
            <name>Pay_by_Credit_Card</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Application__c.Preferred_Deposit_Method__c</field>
            <operation>equals</operation>
            <value>Credit Card</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
