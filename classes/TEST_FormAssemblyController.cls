@isTest
private class TEST_FormAssemblyController {

    static testMethod void mainFormTest() {
        //first, set our dummy callout result to the status, content type and body that we want to receive for this test
        Test.setMock(HttpCalloutMock.class, new TEST_MockWebService('200','text/html','Body'));
        
        //set up the page with the criteria that we want to test
        PageReference pageRef = Page.Form;
        pageRef.getParameters().put('tfa_form','3');
        Test.setCurrentPage(pageRef);
        
        //instantiate the controller and test
        Test.startTest();
        FormAssemblyController controller = new FormAssemblyController();
        controller.getForm();
        Test.stopTest();
        
        //check that we sent the correct endpoint for this scenario and asked for the form
        system.assertEquals(true,controller.endpoint.contains('view'));
        //check that we got the status and body that we were expecting (the callout was performed)
        system.assertEquals('200',controller.res.getStatus());
        system.assertEquals('Body',controller.res.getBody());
    }
    
    static testMethod void mainFormWithTwoParametersTest() {
        //first, set our dummy callout result to the status, content type and body that we want to receive for this test
        Test.setMock(HttpCalloutMock.class, new TEST_MockWebService('200','text/html','Body'));
        
        //set up the page with the criteria that we want to test
        PageReference pageRef = Page.Form;
        pageRef.getParameters().put('tfa_form','3');
        pageRef.getParameters().put('additionalParam1','parameterValue1');
        pageRef.getParameters().put('additionalParam2','parameterValue2');
        Test.setCurrentPage(pageRef);
        
        //instantiate the controller and test
        Test.startTest();
        FormAssemblyController controller = new FormAssemblyController();
        controller.getForm();
        Test.stopTest();
        
        //check that we sent the correct endpoint for this scenario and asked for the form, and that the additional parameters were added to the endpoint correctly
        system.assertEquals(true,controller.endpoint.contains('view'));
        system.assertEquals(true,controller.endpoint.contains('?'));
        system.assertEquals(true,controller.endpoint.contains('&'));
        
        //check that we got the status and body that we were expecting (the callout was performed)
        system.assertEquals('200',controller.res.getStatus());
        system.assertEquals('Body',controller.res.getBody());
    }
    
    static testMethod void mainFormWithOneParametersTest() {
        //first, set our dummy callout result to the status, content type and body that we want to receive for this test
        Test.setMock(HttpCalloutMock.class, new TEST_MockWebService('200','text/html','Body'));
        
        //set up the page with the criteria that we want to test
        PageReference pageRef = Page.Form;
        pageRef.getParameters().put('tfa_form','3');
        pageRef.getParameters().put('additionalParam','parameterValue');
        Test.setCurrentPage(pageRef);
        
        //instantiate the controller and test
        Test.startTest();
        FormAssemblyController controller = new FormAssemblyController();
        controller.getForm();
        Test.stopTest();
        
        //check that we sent the correct endpoint for this scenario and asked for the form, and that the additional parameter was added to the endpoint correctly
        system.assertEquals(true,controller.endpoint.contains('view'));
        system.assertEquals(true,controller.endpoint.contains('?'));
        system.assertEquals(false,controller.endpoint.contains('&'));
        
        //check that we got the status and body that we were expecting (the callout was performed)
        system.assertEquals('200',controller.res.getStatus());
        system.assertEquals('Body',controller.res.getBody());
    }
    
    static testMethod void postSubmitPageTest() {
        //first, set our dummy callout result to the status, content type and body that we want to receive for this test
        Test.setMock(HttpCalloutMock.class, new TEST_MockWebService('200','text/html','Body'));
        
        //set up the page with the criteria that we want to test
        PageReference pageRef = Page.Form;
        pageRef.getParameters().put('tfa_next','someURL');
        Test.setCurrentPage(pageRef);
        
        //instantiate the controller and test
        Test.startTest();
        FormAssemblyController controller = new FormAssemblyController();
        controller.getForm();
        Test.stopTest();
        
        //check that we sent the correct endpoint for this scenario and did not ask for the form
        system.assertEquals(false,controller.endpoint.contains('view'));
        //check that we got the status and body that we were expecting (the callout was performed)
        system.assertEquals('200',controller.res.getStatus());
        system.assertEquals('Body',controller.res.getBody());
    }

}