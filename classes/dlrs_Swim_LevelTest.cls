/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_Swim_LevelTest
{
    @IsTest
    private static void testTrigger()
    {
        // Force the dlrs_Swim_LevelTrigger to be invoked, fails the test if org config or other Apex code prevents this.
        dlrs.RollupService.testHandler(new Swim_Level__c());
    }
}