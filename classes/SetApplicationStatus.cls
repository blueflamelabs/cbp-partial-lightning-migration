public class SetApplicationStatus implements Database.Batchable<sObject> {
    private Id idCampYear;
   
    public SetApplicationStatus(Id idCampYear) {
        this.idCampYear = idCampYear;
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator('SELECT Id, Name, Registration_Type__c, Status__c, Priority_Group__c, Camp_Year__r.Bulk_Assignment_Priority_Groups__c, Camp_Year__r.Exclude_session_assignment__c, C_P_Deposit_Authorization_Transaction__c, Check_Received__c, '  +
                                         '(SELECT Id, Status__c, Application__c, Break_session_calc__c, Eligible__c, Num_Consecutive_Sessions_Needed__c, Registration_Type__c, ' +
                                          'Session__c, Session__r.Maximum_Batch_Assignment__c, Session__r.Session_Capacity__c ' +
                                          'FROM Session_Assignments__r ' +
                                          'ORDER BY Registration_Preference_Calc__c ASC, Session_Preference__c ASC, Session__r.Session_Start_Date__c ASC) ' +
                                        'FROM Application__c WHERE Camp_Year__c = :idCampYear  AND (NOT Status__c IN (\'Accepted\', \'Canceled\')) ' +
                                        'ORDER BY Priority_Group__c ASC, Application_Date_Time__c ASC');
    }

    public void execute(Database.BatchableContext BC, List<Application__c> scope) {
      
        Map<Id, Session_Assignments__c> sessionAssignmentsToUpdate = new Map<Id, Session_Assignments__c>();
        Map<Id, Application__c> applicationsToUpdate = new Map<Id, Application__c>();
        Map<Id, Integer> spotsFilled = new Map<Id, Integer>();
        for (Application__c app : scope) {
            System.debug('' + app.Name);
            if (app.Camp_Year__r.Exclude_session_assignment__c && app.C_P_Deposit_Authorization_Transaction__c == null && !app.Check_Received__c) {
                continue; 
              //applicationsToUpdate.put(app.Id, new Application__c(Id = app.Id, Status__c = 'Accepted'));    
            } else if (app.Priority_Group__c != null && (String.isEmpty(app.Camp_Year__r.Bulk_Assignment_Priority_Groups__c) || app.Camp_Year__r.Bulk_Assignment_Priority_Groups__c.contains(app.Priority_Group__c))) {
                Integer consecutiveSessions = 0;
                Integer approvedSessions = 0;
                for (Integer i=0; i<app.Session_Assignments__r.size(); i++) {
                    Session_Assignments__c sa = app.Session_Assignments__r[i];
                    if (i > 0 && sa.Registration_Type__c == app.Session_Assignments__r[i-1].Registration_Type__c) {
                        consecutiveSessions++;
                    } else {
                        consecutiveSessions = 0;
                    }
                
                    if (consecutiveSessions == 0 && sa.Break_session_calc__c) {
                        sessionAssignmentsToUpdate.put(sa.Id, new Session_Assignments__c(Id = sa.Id, Status__c = 'Waitlist', Waitlist_Reason__c = 'Non-Consecutive'));
                    } else if (!sa.Eligible__c) {
                        sessionAssignmentsToUpdate.put(sa.Id, new Session_Assignments__c(Id = sa.Id, Status__c = 'Waitlist', Waitlist_Reason__c = 'Not Eligible'));
                    } else {
                        Decimal capacity = sa.Session__r.Maximum_Batch_Assignment__c != null ? sa.Session__r.Maximum_Batch_Assignment__c : sa.Session__r.Session_Capacity__c;
                        Decimal spotsUsed = spotsFilled.containsKey(sa.Session__c) ? spotsFilled.get(sa.Session__c) : 0;
                        if (spotsUsed >= capacity) { // space not available in session
                            sessionAssignmentsToUpdate.put(sa.Id, new Session_Assignments__c(Id = sa.Id, Status__c = 'Waitlist', Waitlist_Reason__c = 'No Room'));
                        } else {
                            sessionAssignmentsToUpdate.put(sa.Id, new Session_Assignments__c(Id = sa.Id, Status__c = 'Approved'));
                            approvedSessions++;
                            spotsFilled.put(sa.Session__c, spotsFilled.containsKey(sa.Session__c) ? spotsFilled.get(sa.Session__c) + 1 : 1);
                            if (approvedSessions >= sa.Num_Consecutive_Sessions_Needed__c) {
                                for (Integer j=i+1; j<app.Session_Assignments__r.size(); j++) {
                                    sessionAssignmentsToUpdate.put(app.Session_Assignments__r[j].Id, new Session_Assignments__c(Id = app.Session_Assignments__r[j].Id, Status__c = 'Canceled'));
                                }
                                applicationsToUpdate.put(app.Id, new Application__c(Id = app.Id, Status__c = 'Accepted', Registration_Type__c = sa.Registration_Type__c));
                                break;
                            }
                        }
                    }
                }
                if (!applicationsToUpdate.containsKey(app.Id)) { // application not accepted => change to waitlist
                    applicationsToUpdate.put(app.Id, new Application__c(Id = app.Id, Status__c = 'Waitlist'));
                    for (Session_Assignments__c sa : app.Session_Assignments__r) {
                        if (!sessionAssignmentsToUpdate.containsKey(sa.Id) || sessionAssignmentsToUpdate.get(sa.Id).Status__c != 'Waitlist') {
                            sessionAssignmentsToUpdate.put(sa.Id, new Session_Assignments__c(Id = sa.Id, Status__c = 'Waitlist', Waitlist_Reason__c = 'Non-Consecutive'));
                        }
                    }
                }
            }    
        }
        update sessionAssignmentsToUpdate.values();
        update applicationsToUpdate.values();
		
    }

    public void finish(Database.BatchableContext BC) {
       if(idCampYear != null) {
            //060919 - T-00601 - VennScience_BFL_Monali - query for related Camp Year to get the Camp Name.
            Camp_Year__c campYear = [SELECT Id, 
                                     Name
                                     FROM Camp_Year__c
                                     WHERE Id = :idCampYear];
            // 040919 - T-00601 - VennScience_BFL_Monali - get the Dynamic Url
            String currentUrl = URL.getSalesforceBaseUrl().toExternalForm();
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(new List<String>{UserInfo.getUserEmail()});
            //mail.setReplyTo('support@acme.com');
            mail.setSenderDisplayName('Salesforce Support');
            //060919 - T-00601 - VennScience_BFL_Monali - Commented code to change the Camp Year Name.
            //mail.setSubject('Application Status Updated for Camp Year: ' + idCampYear);
            mail.setSubject('Application Status Updated for Camp Year: ' + campYear.Name);
            mail.setBccSender(false);
            mail.setUseSignature(false);
			
            /* 040919 - T-00601 - VennScience_BFL_Monali - updated the hardcoded url into dynamic url
			 * hardcoded url = https://cs50.salesforce.com/
             * Dynamic url = currentUrl
             */
            //060919 - T-00601 - VennScience_BFL_Monali - updated campId to Camp Year Name.
            //mail.setHtmlBody('<p>The process of updating application statuses for Camp Year ' + idCampYear + ' has just finished.</p>' +
            // '<p>'+'To review the updated data <a href='+currentUrl+'/'+ idCampYear +' target="_blank">click here.</a></p>');
           
             mail.setHtmlBody('<p>The process of updating application statuses for Camp Year ' + campYear.Name + ' has just finished.</p>' +
                             '<p>'+'To review the updated data <a href='+currentUrl+'/'+ idCampYear +' target="_blank">click here.</a></p>');
            Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{mail});
        
        }
    }
}