@isTest
public class TestSetApplicationStatus {
    private static Integer DATA_AMOUNT = 10;
    
    public static testMethod void start(){
        Camp_Year__c campYear1 = new Camp_Year__c( Name = 'Camp year test', Bulk_Assignment_Priority_Groups__c = 'Priority Group 1');
        insert campYear1;        
        
        Contact contact1 = new Contact(LastName = 'Test Last Name', Birthdate = Date.newInstance(2005, 1, 1), Priority_Group__c = 'Priority Group 1');
        insert contact1;

        Map<Integer, Sessions__c> sessions = new Map<Integer, Sessions__c>();
        for(Integer i = 0; i<DATA_AMOUNT; i++){
            sessions.put(i, new Sessions__c(Name = 'Session test ' + i, Maximum_Batch_Assignment__c = 6, Six_week_break_session__c = true, Eight_week_break_session__c = true));
        }
        insert sessions.values();
               
        Map<Integer, Application__c> applications = new Map<Integer, Application__c>();
        for (Integer i = 0; i<DATA_AMOUNT; i++){     
            if (Math.mod(i, 4)==0) {
                applications.put(i, new Application__c(Name = 'Application name test ' + i, Camper__c = contact1.Id, Camp_Year__c = campYear1.Id, Status__c = 'Accepted')); 
            } else {
                applications.put(i, new Application__c(Name = 'Application name test ' + i, Camper__c = contact1.Id, Camp_Year__c = campYear1.Id, Status__c = 'Waitlist'));
            }
        }
        insert applications.values();
        
        List<Session_Assignments__c> sessionsAssigments = new List<Session_Assignments__c>();
        for(Integer i = 0; i<DATA_AMOUNT; i++){
            for(Integer j = 0; j<4; j++){
                if((Math.mod(i, 5)==4 && i!=0) && Math.mod(i, 4)!=0){
                    sessionsAssigments.add(new Session_Assignments__c(Session__c = sessions.get(i).Id, Application__c = applications.get(i).Id, Session_Preference__c = j+1 + '', Registration_Type__c = 'Eight-week Camper', Status__c = 'Accepted'));
                }else{
                    sessionsAssigments.add(new Session_Assignments__c(Session__c = sessions.get(i).Id, Application__c = applications.get(i).Id, Session_Preference__c = j+1 + '', Registration_Type__c = 'Six-week Camper', Status__c = 'Aproved'));
                }      
            }
        }
        insert sessionsAssigments;  
        
        Test.startTest();
        Database.executeBatch(new SetApplicationStatus(campYear1.Id));
        Test.stopTest();
        
        Map<Id, Application__c> applicationsUpdate = new Map<Id, Application__c>([SELECT Id, Status__c FROM Application__c WHERE ID IN :applications.values()]);
        
        for(Integer i = 0; i<DATA_AMOUNT; i++){
            if(Math.mod(i, 5)==4  && i!=0){
               if(Math.mod(i, 4)==0){
                   //each forthy applicationa has status 'Accepted'
                   System.assertEquals('Accepted', applicationsUpdate.get(applications.get(i).Id).Status__c); 
               } else {
                   //Num_Consecutive_Sessions_Needed__c is bigger than count Session_Assignments__c
                   System.assertEquals('Waitlist', applicationsUpdate.get(applications.get(i).Id).Status__c);
               }
            } else {
                System.assertEquals('Accepted', applicationsUpdate.get(applications.get(i).Id).Status__c);
            }            
        }
    }
}