@isTest
public class TestDataFactory {
    
    public static Camp_Year__c getcampYear() {
        Camp_Year__c campyearObj = new Camp_Year__c();
        campyearObj.Name = 'Test camp year';
        campyearObj.Bulk_Assignment_Priority_Groups__c = 'Priority Group 2';
        campyearObj.Exclude_session_assignment__c = false;
        return campyearObj;
    }
    
    public static Application__c getApplications(Id campyearId, String bulkassignment, Boolean excludeAssignment) {
        Application__c appObj = new Application__c();
        appObj.Name = 'Test Application';
        appObj.Registration_Type__c = 'Two-week Camper';
        appObj.Status__c = 'Applied';
        appObj.Camp_Year__c = campyearId;
        appObj.Camp_Year__r.Bulk_Assignment_Priority_Groups__c = bulkassignment;
        appObj.Camp_Year__r.Exclude_session_assignment__c = excludeAssignment;
        appObj.C_P_Deposit_Authorization_Transaction__c = '';
        appObj.Check_Received__c = false;
        
        return appObj;
    }
    
    public static Session_Assignments__c getSessionAssignments(Id campYearId) {
        Session_Assignments__c sessionAssign = new Session_Assignments__c();
        sessionAssign.Camp_Year__c = campYearId;
        return sessionAssign;
    }
    
    public static CnP_PaaS__CnP_Transaction__c getCPTransaction() {
        CnP_PaaS__CnP_Transaction__c cpTransaction = new CnP_PaaS__CnP_Transaction__c();
        cpTransaction.CnP_PaaS__Application_Name__c = 'Test Application';
        return cpTransaction;
    }
   
}