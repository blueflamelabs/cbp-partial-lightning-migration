global class FormAssemblyController {
	/* Created by KELL Partners 2015
     * Based on sample code provided by FormAssembly (https://help.formassembly.com/help/article/link/354549-using-formassemblys-publishing-api-with-salesforce)
     * 
     * universal formassembly controller for visualforce
     * Given a URL parameter tfa_form=XXX with the formId, displays the form using the FormAssembly REST API
     * 
     */ 
    
    public HTTPResponse res {get; set;}
    public String resBody {get; set;}
    public String endpoint {get; set;}
    public String URLbase;
    Map<String, String> paramList;

    public FormAssemblyController() {
        PageReference pageRef = ApexPages.currentPage();
        paramList = pageRef.getParameters();

        //Set the first part of the url for the client. generally https://app.formassembly.com/rest, but can be different for groups with enterprise level FA contracts
        URLbase = 'https://app.formassembly.com/rest';         

        if (paramList.containsKey('tfa_next')) {
            //the tfa_next parameter sets the post-submit page. Go to the page, don't call the form body
            endpoint = URLbase + pageRef.getParameters().get('tfa_next');
        } else if (paramList.containsKey('tfa_form')) {
            //if the formId is specified, call the form using the API
            endpoint = URLbase + '/forms/view/' + pageRef.getParameters().get('tfa_form');
            //check to see if there are additional parameters
            String additionalParams = '';
            for (String param : paramList.keySet()) {
                if(param != 'tfa_form') {
                    if(additionalParams.length() > 2) {
                        //add a separator if this isn't the first additional parameter
                        additionalParams+='&';
                    }
                    additionalParams+=param+'='+paramList.get(param);
                }
            }
            
            if (additionalParams.length() > 2) {
                //if there are additional parameters in the URL, append them to the endpoint
                endpoint +='?'+additionalParams;
            }
        } else {
            //error conditions here, this is not directing to a specific form nor is it a post-submit page
        }
        
        //set the endpoint or URL for our request
        system.debug('Endpoint: '+endpoint);                       
    }
    
    public void getForm() {
        //Create an HTTP request,
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        if (endpoint <> null) {  
            //if an endpoint has been defined in the constructor, set it and send the request
            req.setEndpoint(endpoint);
            
            //send the HTTP request
        	Http http = new Http();
        	try {
            	res = http.send(req);
            	resBody = res.getBody(); //this is the variable that holds the HTML for the FormAssembly Form
        	} catch(System.CalloutException e) {
            	//Exception handling goes here....
            	System.debug(e);
        	}
        }
        //if the endpoint was never defined in the constructor, the request is not sent
        
    }
    
}