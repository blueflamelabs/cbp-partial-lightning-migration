@isTest
public class SetUpApplicationControllerTest {
    
    @isTest
    public static void setUpApplicationTest() {
        
        Camp_Year__c campYear = TestDataFactory.getcampYear();
        insert campyear;
        
        Test.startTest();
        Id jobId = SetUpApplicationController.getCampYear(campyear.Id);
        System.assert(jobId != null);
        Test.stopTest();
    }
    
    @isTest
    public static void setUpApplicationTestWithoutCampYear() {
        
        Camp_Year__c campYear = TestDataFactory.getcampYear();
                
        Test.startTest();
        Id jobId = SetUpApplicationController.getCampYear(null);
        
        System.assert(jobId != null);
        Test.stopTest();
    }
    
    @isTest
    public static void setUpApplicationTestPicklistVales() {
        
        Camp_Year__c campYear = TestDataFactory.getcampYear();
                
        Test.startTest();
        List<String> picklistValues = SetUpApplicationController.getPiklistValues();
        
        System.assert(picklistValues != null);
        Test.stopTest();
    }
    
    @isTest
    public static void setUpApplicationTestDefaultPicklistVales() {
        
        Camp_Year__c campYear = TestDataFactory.getcampYear();
                
        insert campYear;
        Test.startTest();
        String picklistValues = SetUpApplicationController.getSelectedBulkAssignmentValue(campYear.Id);
        
        System.assert(picklistValues != null);
        Test.stopTest();
    }
    
    @isTest
    public static void setUpApplicationTestUpdatePicklistVales() {
        
        Camp_Year__c campYear = TestDataFactory.getcampYear();
                
        insert campYear;
        String selectedpicklistValues = '["Test1","Test2"]';
        Test.startTest();
        SetUpApplicationController.updateBulkAssignmentValue(campYear.Id, selectedpicklistValues);
        
        Camp_Year__c campYearRecord = [SELECT Id, 
                                           		  Bulk_Assignment_Priority_Groups__c
                                             FROM Camp_Year__c];
        
        System.assert(campYearRecord.Bulk_Assignment_Priority_Groups__c != null);
        String updatedValue = 'Test1;Test2';
        System.assertEquals(updatedValue, campYearRecord.Bulk_Assignment_Priority_Groups__c);
        Test.stopTest();
    }
    
    /*@isTest
    public static void setUpApplicationTestIdNullDefaultPicklistVales() {
        
        Camp_Year__c campYear = TestDataFactory.getcampYear();
        
        Test.startTest();
        String picklistValues = SetUpApplicationController.getSelectedBulkAssignmentValue(null);
        
        System.assert(String.isEmpty(picklistValues));
        Test.stopTest();
    }*/

}