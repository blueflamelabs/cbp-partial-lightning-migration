@isTest
global class TEST_MockWebService implements HttpCalloutMock {    

	public static String responseStatus;
    public static String responseContentType;
    public static String responseBody;
    
    global TEST_MockWebService(String theStatus, String theContentType, String theBody) {
        responseStatus = theStatus;
        responseContentType = theContentType;
        responseBody = theBody;        
    }
    
    global TEST_MockWebService() {
        //default to a successful response containing html if no values are specified in the constructor
        responseStatus = '200';
        responseContentType = 'text/html';
        responseBody = '';
    }

    
    global HTTPResponse respond(HTTPRequest req) {
        HttpResponse res = new HttpResponse();
        //set the status, content type and body of the fake response depending on values provided by the constructor
        res.setStatus(responseStatus);
        res.setHeader('Content-type',responseContentType);
        res.setBody(responseBody);
		system.debug('Response: '+res);        
        return res;
    }
    

}