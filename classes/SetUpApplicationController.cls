/*********************************************************************************************************************************************************
*     Version    CreateDate/Modified     CreatedBy                    Description
*       1.0         050919               VennScience_BFL_Monali       This class is built to call the batch Apex in lightning 
*       1.1         120919               VennScience_BFL_Monali        Added the getPiklistValues method to get the picklist values.
**********************************************************************************************************************************************************/ 
public class SetUpApplicationController {
    
     /**
   * Method Name : getCampYear
   * Description : Used to call the batch Apex.
   * Created By  : VennScience_BFL_Monali 
   * Created On  : 050919
   **/
    @AuraEnabled
    public static Id getCampYear(Id campRecordId) {
         Id jobId = Database.executeBatch(new SetApplicationStatus(campRecordId));
         return jobId;  
    }
    
    /**
   * Method Name : getPiklistValues
   * Description : Used to get the picklist values.
   * Created By  : VennScience_BFL_Monali 
   * Created On  : 120919
   **/
    @AuraEnabled
    public static List <String> getPiklistValues() {
        List<String> plValues = new List<String>();
         
        //Get the object type from object name. Here I've used custom object Camp_Year__c.
        Schema.SObjectType objType = Schema.getGlobalDescribe().get('Camp_Year__c');
         
        //Describe the sObject using its object type.
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
         
        //Get the specific field information from field name. Here I've used custom field Bulk_Assignment_Priority_Groups__c of Camp_Year__c object.
        Schema.DescribeFieldResult objFieldInfo = objDescribe.fields.getMap().get('Bulk_Assignment_Priority_Groups__c').getDescribe();
         
        //Get the picklist field values.
        List<Schema.PicklistEntry> picklistvalues = objFieldInfo.getPicklistValues();
         
        //Add the picklist values to list.
        for(Schema.PicklistEntry plv: picklistvalues) {
            plValues.add(plv.getValue());
        }
        System.debug('plValues  '+plValues);
        plValues.sort();
        return plValues;
    }
    
    /**
   * Method Name : updateBulkAssignmentValue
   * Description : Used to Update the picklist values.
   * Created By  : VennScience_BFL_Monali 
   * Created On  : 120919
   **/
    @AuraEnabled
    public static void updateBulkAssignmentValue(Id campRecordId, String selectedBulkValues) {
        System.debug('campRecordId '+campRecordId);
        System.debug('selectedBulkValues '+selectedBulkValues );
        selectedBulkValues = selectedBulkValues.remove('"');
        selectedBulkValues = selectedBulkValues.replace(',', ';');
        selectedBulkValues = selectedBulkValues.removeStart('[');
        selectedBulkValues = selectedBulkValues.removeEnd(']');
        System.debug('selectedBulkValues==== '+selectedBulkValues);
        //String bulkAssignment1 = bulkAssignment.Split(',');
        //System.debug('bulkAssignment1 '+bulkAssignment1);
        Camp_Year__c campYearRecord = [SELECT Id, 
                                       Bulk_Assignment_Priority_Groups__c
                                       FROM Camp_Year__c
                                       WHERE Id =: campRecordId];
        campYearRecord.Bulk_Assignment_Priority_Groups__c = selectedBulkValues;
        System.debug('campYearRecord '+campYearRecord );
        Update campYearRecord;
    }

    /**
   * Method Name : getSelectedBulkAssignmentValue
   * Description : Used to get the selected the picklist values.
   * Created By  : VennScience_BFL_Monali 
   * Created On  : 120919
   **/
    @AuraEnabled
    public static String getSelectedBulkAssignmentValue(Id campRecordId) {
        System.debug('campRecordId '+campRecordId);
        
        Camp_Year__c campYearRecord = [SELECT Id, 
                                       Bulk_Assignment_Priority_Groups__c
                                       FROM Camp_Year__c
                                       WHERE Id =: campRecordId];
        return campYearRecord.Bulk_Assignment_Priority_Groups__c;
        
    }

}