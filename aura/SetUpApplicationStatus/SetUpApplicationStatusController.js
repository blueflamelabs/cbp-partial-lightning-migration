({
    doInit: function(component, event, helper) {
        helper.getSelectedbulkValue(component, event, helper);
        helper.init(component, event, helper);
        
    },
    
	handleBatchRunVsDisplayMsg : function(component, event, helper) {
        /*var changeElement = component.find("test");
        $A.util.addClass(changeElement, "toggle");*/
		helper.displayMessage(component, event, helper);
        helper.updateBulkAssignmentValues(component, event, helper);
	},
    
    handleBulkAssignmentChange: function (component, event, helper) {
        //Get the Selected values   
        var selectedValues = event.getParam("value");
        console.log('selectedValues',selectedValues);
        //Update the Selected Values  
        component.set("v.selectedGenreList", selectedValues);
    },
    
   closePopUp: function(component, event, helper) {
       $A.get("e.force:closeQuickAction").fire();
   },
    
    closeModel: function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        component.set("v.isOpen", false);
        $A.get("e.force:closeQuickAction").fire();
    },
 	
   closeMessagePopup: function(component, event, helper) {
      component.set("v.isOpen", false);
      $A.get("e.force:closeQuickAction").fire();
      $A.get('e.force:refreshView').fire();
   },

    
   
    
    
})