({
    
    init : function(component, event, helper) {
        var action = component.get("c.getPiklistValues");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS"){
                var result = response.getReturnValue();
                var plValues = [];
                for (var i = 0; i < result.length; i++) {
                    plValues.push({
                        label: result[i],
                        value: result[i]
                    });
                }
                // console.log('plValues ',plValues);
                component.set("v.bulkassignmentList", plValues);
                // console.log('default values ',component.get("v.defaultBulkAssignmentList"));
                var bulkValues = component.get("v.defaultBulkAssignmentList");
                // console.log('bulkValues ',bulkValues);
                component.set("v.selectedBulkAssignmentList",component.get("v.defaultBulkAssignmentList"));
            }
        });
        $A.enqueueAction(action);
        
    },
    
    getSelectedbulkValue : function(component, event, helper) {
        var action = component.get("c.getSelectedBulkAssignmentValue");
        action.setParams({
            "campRecordId" : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if( state === 'SUCCESS'){
                var getBulkAssignmentResponse = response.getReturnValue();
                // console.log('getBulkAssignmentResponse ',getBulkAssignmentResponse);
                if(getBulkAssignmentResponse != null || getBulkAssignmentResponse != undefined){
                    getBulkAssignmentResponse = getBulkAssignmentResponse.split(';');
                    // console.log('getBulkAssignmentResponse ',getBulkAssignmentResponse);  
                }
                
                component.set("v.defaultBulkAssignmentList",getBulkAssignmentResponse);
            }
        });
        $A.enqueueAction(action);
    },
    
    displayMessage : function(component, event, helper) {
        // console.log('run batch');
        //var recordId = component.get("v.recordId");
        //// console.log('recordId ', component.get("v.recordId"));
        var action = component.get("c.getCampYear");
        action.setParams({
            "campRecordId" : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if( state === 'SUCCESS'){
                var getresponse = response.getReturnValue();
                // console.log('getresponse ',getresponse);
                component.set("v.isOpen",true);
                //$A.get("e.force:closeQuickAction").fire();
            }
        });
        $A.enqueueAction(action);
	},
    
    updateBulkAssignmentValues : function(component, event, helper) {
        // console.log('update bulk field value');
        var selectedValues = component.get("v.selectedBulkAssignmentList");
        // console.log('Selectd bulkAssignment-' + selectedValues);
        // console.log('JSON format Selectd bulkAssignment-' + JSON.stringify(selectedValues));
        var action = component.get("c.updateBulkAssignmentValue");
        action.setParams({
            "campRecordId" : component.get("v.recordId"),
            "selectedBulkValues" : JSON.stringify(selectedValues)
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if( state === 'SUCCESS'){
                var getresponse = response.getReturnValue();
                // console.log('update bulk ',getresponse);
                //component.set("v.isOpen",true);
                var successMsg = 'Application status update has just started. As soon as the process is finished you will get a notification email.';
                helper.showMsgToastfire(successMsg);
                $A.get("e.force:closeQuickAction").fire();
            }
        });
        $A.enqueueAction(action);
    },
    
    showMsgToastfire : function(msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Success',
            message: msg,
            duration:'3000',
            type: 'Success'
        });
        toastEvent.fire();
        },
})